export default {
    element: 'ImageProgressiveLoader: Provided element is invalid.',
    image: 'ImageProgressiveLoader: Element image not found.',
    placeholder: 'ImageProgressiveLoader: Element placeholder not found'
}
