'use strict'

import 'intersection-observer'
import objectFitImages from 'object-fit-images'
import ErrorMessages from './messages/error'
import {hasState, setState} from './helpers/State';
import {getElement, getElements} from "./helpers/Element";

class ImageProgressiveLoader {

    constructor({element, loadedStateName = 'loaded'} = {}) {
        this.element = element
        this.loadedStateName = loadedStateName
        this.image = getElement('ipl-image', this.element)

        this.validateParams()
        this.initObserver()
    }

    initObserver() {
        const observer = new IntersectionObserver((entry, observer) => {
            if (!this.isLoaded() && entry[0].isIntersecting) {
                if (this.image.tagName === 'PICTURE') {
                    this.processPicture()
                } else if (this.image.tagName === 'IMG') {
                    this.processImg()
                }

            }
        })

        observer.observe(this.element)
    }

    processPicture() {
        const [...sources] = this.image.querySelectorAll('source')
        const image = this.image.querySelector('img')

        if (!image) throw new Error(ErrorMessages.image)

        sources.push(image)

        sources.forEach(this.changeSrc)

        objectFitImages(image)
        image.addEventListener('load', () => this.onImageLoad())
    }

    processImg() {
        this.changeSrc(this.image)

        objectFitImages(this.image)
        this.image.addEventListener('load', () => this.onImageLoad())
    }

    changeSrc(el) {
        const src = el.dataset.src || el.dataset.srcset

        if (el.dataset.src) {
            el.removeAttribute('data-src')
            el.src = src
        } else if (el.dataset.srcset) {
            el.removeAttribute('data-srcset')
            el.srcset = src
        }
    }

    onImageLoad() {
        setState(this.element, this.loadedStateName)
    }

    isLoaded() {
        return hasState(this.element, this.loadedStateName)
    }

    validateParams() {
        if (!this.element) throw new Error(ErrorMessages.element)
        if (!this.image) throw new Error(ErrorMessages.image)
    }

    static initGlobal(selector = 'ipl') {
        const elements = getElements(selector)

        elements.forEach((element) => new ImageProgressiveLoader({element: element}))
    }
}

export default ImageProgressiveLoader
