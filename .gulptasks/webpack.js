/* eslint-disable */
import path from 'path';
import webpack from 'webpack';
import process from 'process';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';

const isProduction = (process.env.NODE_ENV === 'production');

let config = {

    mode: isProduction ? 'production' : 'development',
    context: path.resolve(__dirname, './'),
    watch: true,

    entry: path.resolve(__dirname,'../example/js/main.js'),

    output: {
        filename: '../example/js/build.js',
        path: path.resolve(__dirname, './'),
        publicPath: '/'
    },

    resolve: {
        modules: [
            path.resolve('node_modules'),
        ],
        extensions: ['.js', '.jsx', '.json'],
    },

    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },

    optimization: {
        minimizer: isProduction ? [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true,
            })
          ] : []
    },

    plugins: isProduction ? [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"',
        }),
    ] : []
};

const javascripts = () => {

    return new Promise(resolve => webpack(config, (err) => {
        if (err) console.log('Webpack', err);

        resolve();
    }));
};

module.exports = { config, javascripts };
