/* eslint-disable */
import gulp from 'gulp';

// gulp modules import
import { clean } from './.gulptasks/misc';
import { javascripts } from './.gulptasks/webpack';
import { stylesheets } from './.gulptasks/styles';
import { server } from './.gulptasks/server';

export const dev = gulp.series(
    clean,
    gulp.parallel(
        javascripts,
        stylesheets
    ),
    server
);

export const build = gulp.series(
    clean,
    gulp.parallel(
        javascripts,
        stylesheets
    )
);

export default dev;
