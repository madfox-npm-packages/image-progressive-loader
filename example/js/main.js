import ImageProgressiveLoader from "../../src";

const App = (() => {

    const init = () => {
        ImageProgressiveLoader.initGlobal()
    }

    return {
        init
    }
})()

if (document.readyState === 'loading') {
    window.addEventListener('DOMContentLoaded', App.init)
} else {
    App.init()
}
